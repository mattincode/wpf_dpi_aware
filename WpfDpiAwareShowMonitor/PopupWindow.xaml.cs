﻿using System.Timers;
using System.Windows;

namespace WpfDpiAwareShowMonitor
{
    /// <summary>
    /// Interaction logic for PopupWindow.xaml
    /// </summary>
    public partial class PopupWindow : Window
    {
        Timer _timer;
        public PopupWindow(int number)
        {
            InitializeComponent();
            monitorNumberTxt.Text = number.ToString();
            _timer = new Timer(3000);
            _timer.Elapsed += _timer_Elapsed;
            _timer.Start();
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();
            _timer = null;
            Application.Current.Dispatcher.Invoke(() =>
            {
                Close();
            });            
        }
    }
}
