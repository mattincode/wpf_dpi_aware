﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfDpiAwareShowMonitor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void onIdentifyBtnClick(object sender, RoutedEventArgs e)
        {
            //Ref: https://stackoverflow.com/questions/49051461/how-to-change-dpi-scaling-behavior-in-c-sharp/49120053#49120053
            int screenNo = 0;
            // Below code from; https://stackoverflow.com/questions/30933787/wpf-create-one-window-for-each-screen-and-center-them-on-each-screen
            foreach (var s in Screen.AllScreens)
            {
                screenNo++;
                var b = s.Bounds;
                var w = new PopupWindow(screenNo);

                var oW = w.Width; //Keep track of original size ...
                var oH = w.Height;

                w.Width = 0; //then set the size to 0, to avoid that the 
                w.Height = 0;//popup shows before it is correctly positioned

                w.Show(); //Now show it, so that we can place it (when I
                          //tried to place it before showing, the window
                          //was always repositioned when Show() was called)
                          
                double dpiX = 1, dpiY = 1;
                var ps = PresentationSource.FromVisual(w);
                if (ps != null)
                {
                    dpiX = ps.CompositionTarget.TransformToDevice.M11;
                    dpiY = ps.CompositionTarget.TransformToDevice.M22;
                }
                
                var aW = oW * dpiX; //Calculate the actual size of the window
                var aH = oH * dpiY;

                w.Left = (int)((b.X + (b.Width - aW) / 2) / dpiX);
                w.Top = (int)((b.Y + (b.Height - aH) / 2) / dpiX);

                w.Width = oW; //And set the size back to the original size
                w.Height = oH;
                
            }
        }
    }
}
